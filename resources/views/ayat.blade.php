<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel = "icon" href = {{'../quran_icon_black.svg'}} type = "image/x-icon">
    <title>Quran</title>
    @vite('resources/css/app.css')
    @livewireStyles
</head>
<body class="bg-custom-white relative min-h-screen">
    <x-nav/>

    @livewire('ayat', ['surah_id' => $surah_id])

    <x-footer/>
    @livewireScripts
</body>
</html>