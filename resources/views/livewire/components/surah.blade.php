<div class="col-span-12 sm:col-span-6 xl:col-span-4">
    <div class="">
        <div wire:click='readSurah({{$surahId}})' class='group bg-custom-white motion-safe:fadeIn w-80 group inline-flex gap-4 px-2 py-2 shadow-sm border-solid border-2 border-custom-grey hover:border-spacing-3 hover:border-custom-yellow hover:bg-custom-black cursor-pointer rounded-lg'>
            <div class='w-16 h-12 inline-flex justify-center items-center bg-custom-grey group-hover:bg-custom-yellow rounded-2xl self-center transition duration-300 group-hover:rotate-45'>
                <p class='font-semibold text-lg self-center group-hover:-rotate-45 text-custom-white group-hover:text-custom-black transition duration-300'>{{$surahId}}</p>
            </div>
            <div class="grid grid-rows-2 gap-2 w-full">
                <div class="grid grid-cols-12 justify-items-stretch">
                    <p class='col-span-6 font-semibold text-sm text-custom-black group-hover:text-custom-yellow'>{{$surahLatin}}</p>
                    <p id="arab" class='col-span-6 pr-4 font-semibold text-base text-custom-black group-hover:text-custom-yellow'>{{$surahArabic}}</p>
                </div>
                <div>
                    <p class='font-light text-xs italic text-custom-black group-hover:text-custom-yellow'>{{$surahTerjemah}}</p>
                </div>
            </div>
        </div>
    </div>
</div>