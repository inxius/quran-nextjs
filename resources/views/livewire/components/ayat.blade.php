<div class="w-full px-6 sm:px-16 lg:px-48 py-2">
    <div class="group/main grid grid-cols-1 sm:grid-cols-12 gap-2 py-4 border-b-2 border-custom-grey/40 hover:border-custom-black/50">
        <div class="w-full col-span-1 inline-flex justify-center">
            <div class="self-start sm:justify-self-center w-[41px] h-[48px] bg-border-ayat bg-no-repeat bg-center bg-contain flex justify-center items-center">
                <p class="font-normal text-sm text-custom-black">{{$ayatNumber}}</p>
            </div>
        </div>
        <div class="sm:col-span-11">
            <div class="pb-6 border-b-2 border-custom-grey/50 group-hover/main:border-custom-black/50">
                <p id="arab" class="font-normal text-2xl text-custom-black leading-loose">{{$ayatArabic}}</p>
            </div>
            <div class="w-full inline-flex gap-2 items-start justify-center sm:justify-start pt-2">
                <audio id={{'arabicAudio'.$ayatNumber}}>
                    <source src={{$ayatAudio}} type="audio/mpeg">
                </audio>
                <button 
                    type="button"
                    onclick="playAudio({{'arabicAudio'.$ayatNumber}})"
                    class="group/play bg-custom-grey py-1 px-2 rounded-md hover:bg-custom-black"
                >
                    <svg class="w-6 fill-custom-white p-1 group-hover/play:fill-custom-yellow" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M21.4086 9.35258C23.5305 10.5065 23.5305 13.4935 21.4086 14.6474L8.59662 21.6145C6.53435 22.736 4 21.2763 4 18.9671L4 5.0329C4 2.72368 6.53435 1.26402 8.59661 2.38548L21.4086 9.35258Z"></path> </g></svg>
                </button>
                <button 
                    type="button"
                    onclick="pauseAudio({{'arabicAudio'.$ayatNumber}})"
                    class="group/play bg-custom-grey py-1 px-2 rounded-md hover:bg-custom-black"
                >
                    <svg class="w-6 fill-custom-white group-hover/play:fill-custom-yellow" fill="#ffffff" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <title>pause</title> <path d="M5.92 24.096q0 0.832 0.576 1.408t1.44 0.608h4.032q0.832 0 1.44-0.608t0.576-1.408v-16.16q0-0.832-0.576-1.44t-1.44-0.576h-4.032q-0.832 0-1.44 0.576t-0.576 1.44v16.16zM18.016 24.096q0 0.832 0.608 1.408t1.408 0.608h4.032q0.832 0 1.44-0.608t0.576-1.408v-16.16q0-0.832-0.576-1.44t-1.44-0.576h-4.032q-0.832 0-1.408 0.576t-0.608 1.44v16.16z"></path> </g></svg>
                </button>
                <button 
                    wire:click='toogleTerjemah' 
                    class="group/terjemah bg-custom-grey py-1 px-2 rounded-md hover:bg-custom-black"
                >
                    <p class="text-custom-white group-hover/terjemah:text-custom-yellow">{{$showTerjemah ? "Tutup Terjemahan" : "Buka Terjemahan"}}</p>
                </button>
            </div>
            @if ($showTerjemah)
                <div>
                    <p class="pt-6 font-thin text-sm text-custom-black italic group-hover:font-extralight">
                        {{$ayatTerjemahan}}
                    </p>
                </div>
            @endif
        </div>
        <script>
            function playAudio(audioId) { 
                audioId.play();
            }

            function pauseAudio(audioId) {
                audioId.pause();
            }
        </script>
    </div>
</div>