<div class='grid grid-cols-12 gap-3 justify-items-center px-4 sm:px-20 lg:px-32 pt-5 pb-20'>
    @foreach ($surah as $surah)
        @livewire('components.surah', ['surahId' => $surah['number'], 'surahLatin' => $surah['englishName'], 'surahArabic' => $surah['name'], 'surahTerjemah' => $surah['englishNameTranslation']])
    @endforeach
</div>