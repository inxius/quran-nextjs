<div class="flex flex-col justify-center mt-2 pb-20">
    <div class="flex flex-col justify-center items-center mb-6">
        <p id="arab" class="font-normal text-custom-black text-2xl py-2 leading-loose">{{$surah['name']}}</p>
        <p class="font-medium text-custom-black text-base">{{$surah['englishName']}}</p>
        <p class="font-extralight text-custom-black text-sm italic">{{$surah['englishNameTranslation']}}</p>
        <p class="font-light text-custom-black text-sm">Jumlah Ayat : {{$surah['numberOfAyahs']}}</p>
    </div>
    @foreach ($ayat as $ayat)
        @livewire('components.ayat', ['surahNumber' => $surah['number'], 'ayatNumber' => $ayat['numberInSurah'], 'ayatArabic' => $ayat['text'], 'ayatAudio' => $ayat['audio']])
    @endforeach
</div>
