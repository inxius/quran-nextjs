<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel = "icon" href = {{'quran_icon_black.svg'}} type = "image/x-icon">
    <title>Quran</title>
    @vite('resources/css/app.css')
</head>
<body class="">
    <div class="absolute h-screen w-screen bg-custom-black top-0 z-20">
        <div class="h-full flex flex-col gap-2 justify-center items-center p-8">
            <svg class="w-48" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M8.976 21C4.05476 21 3 19.9452 3 15.024" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M21 15.024C21 19.9452 19.9452 21 15.024 21" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M15.024 3C19.9452 3 21 4.05476 21 8.976" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M8 16C8.91221 14.7856 10.3645 14 12.0004 14C13.6362 14 15.0885 14.7856 16.0007 16" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M9 10.0112V10" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M15 10.0112V10" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> <path d="M3 8.976C3 4.05476 4.05476 3 8.976 3" stroke="#f8d210" stroke-width="2" stroke-linecap="round"></path> </g></svg>
            <p class="text-custom-yellow text-lg font-bold">Sorry, There is some error</p>
            <a href="/">
                <button class="bg-custom-yellow py-1 px-2 rounded-md">
                    <p class="text-custom-black font-medium">Home</p>
                </button>
            </a>
        </div>
    </div>
</body>
</html>