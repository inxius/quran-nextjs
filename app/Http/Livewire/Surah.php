<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\Surah\GetSurah;

class Surah extends Component
{
    public $surah;

    public function mount(GetSurah $getSurahAction)
    {
        $this->surah = $getSurahAction->execute();
    }

    public function readSurah(int $surahId)
    {
        return redirect()->to('/surah/'.$surahId);
    }

    public function render()
    {
        if (!$this->surah) {
            return view('errors.404');
        }
        return view('livewire.surah');
    }
}
