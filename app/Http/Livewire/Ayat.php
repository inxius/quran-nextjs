<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\Surah\GetDetailSurah;

class Ayat extends Component
{
    public $surah_id;
    public $surah;
    public $ayat;

    public function mount(GetDetailSurah $detailSurahAction)
    {
        $this->surah = $detailSurahAction->execute($this->surah_id);
        if ($this->surah) {
            $this->ayat = $this->surah['ayahs'];
        }
    }

    public function render()
    {
        if (!$this->surah) {
            return view('errors.404');
        }
        return view('livewire.ayat');
    }
}
