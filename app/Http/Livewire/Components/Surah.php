<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class Surah extends Component
{
    public int $surahId;
    public string $surahLatin;
    public string $surahArabic;
    public string $surahTerjemah;

    public function readSurah(int $surahId)
    {
        return redirect()->to('/surah/'.$surahId);
    }

    public function render()
    {
        return view('livewire.components.surah');
    }
}
