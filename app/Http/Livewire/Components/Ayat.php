<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;
use App\Actions\Ayat\GetTranslate;

class Ayat extends Component
{
    public $surahNumber;
    public $ayatNumber;
    public $ayatArabic;
    public $ayatTerjemahan = false;
    public $ayatAudio;
    public $showTerjemah = false;

    public function toogleTerjemah(GetTranslate $translateAction) : void {
        if (!$this->ayatTerjemahan) {
            $this->getTerjemah($translateAction);
        }
        $this->showTerjemah = !$this->showTerjemah;
    }

    public function getTerjemah(GetTranslate $translateAction) : void {
        $this->ayatTerjemahan = $translateAction->execute($this->surahNumber, $this->ayatNumber);
    }

    public function render()
    {
        return view('livewire.components.ayat');
    }
}
