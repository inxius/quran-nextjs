<?php

namespace App\Actions\Surah;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class GetDetailSurah
{
    public function execute(int $surahId) : array|bool {
        $key = 'surah-'.$surahId;

        if (Cache::get($key)) {
            return Cache::get($key);
        } else {
            $response = Http::withUrlParameters([
                'endpoint' => 'http://api.alquran.cloud/v1/surah',
                'surahId' => $surahId,
                'edition' => 'ar.shaatree',
            ])->get('{+endpoint}/{surahId}/{edition}');
    
            if ($response->ok()) {
                $res = $response->collect();
                Cache::put($key, $res['data'], now()->addDay(30));
                return $res['data'];
            } else {
                return false;
            }
        }
    }
}
