<?php

namespace App\Actions\Surah;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class GetSurah
{
    public function execute() : array|bool {
        if (Cache::get('surah')) {
            return Cache::get('surah');
        } else {
            $response = Http::get('http://api.alquran.cloud/v1/surah');

            if ($response->ok()) {
                $res = $response->collect();
                Cache::put('surah', $res['data'], now()->addDay(30));
                return $res['data'];
            } else {
                return false;
            }
        }
    }
}
