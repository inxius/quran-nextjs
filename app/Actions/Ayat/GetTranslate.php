<?php

namespace App\Actions\Ayat;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class GetTranslate
{
    public function execute(int $surahNumber, int $ayatNumber) : string {
        $key = 'translate-'.$surahNumber.'-'.$ayatNumber;

        if (Cache::get($key)) {
            return Cache::get($key);
        } else {
            $response = Http::withUrlParameters([
                'endpoint' => 'http://api.alquran.cloud/v1/ayah',
                'reference' => $surahNumber.':'.$ayatNumber,
                'edition' => 'id.indonesian',
            ])->get('{+endpoint}/{reference}/{edition}');
    
            if ($response->ok()) {
                $res = $response->collect();
                Cache::put($key, $res['data']['text'], now()->addDay(30));
                return $res['data']['text'];
            } else {
                return "Sorry, there is some errors!";
            }
        }
    }
}
