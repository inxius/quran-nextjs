<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Surah extends Model
{
    use HasFactory;

    protected $table = 'table_surah';

    /**
     * Get the ayats for the surah.
     */
    public function ayats(): HasMany
    {
        return $this->hasMany(Ayat::class);
    }
}
