/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    colors:{
      transparent: 'transparent',
      current: 'currentColor',
      'custom-black' : '#180f21',
      'custom-grey' : '#677b8c',
      'custom-yellow' : '#f8d210',
      'custom-white' : '#ffffff',
    },
    extend: {
      backgroundImage: {
        'border-ayat': "url('/public/ayat-border.svg')",
      },
    },
  },
  plugins: [],
}

